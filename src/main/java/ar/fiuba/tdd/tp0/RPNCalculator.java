package ar.fiuba.tdd.tp0;

import java.util.Hashtable;
import java.util.LinkedList;

public class RPNCalculator {

    interface MultipleMathOperation {
        float operationMultiple(LinkedList<Float> pila);
    }

    private float operateMultiple(LinkedList<Float> pila, MultipleMathOperation multipleMathOperation) {
        return multipleMathOperation.operationMultiple(pila);
    }

    Hashtable<String, MultipleMathOperation> operadores;
    LinkedList<Float> pilaNumeros;


    MultipleMathOperation sumaMultiple = (LinkedList<Float> pilaNumeros) -> {
        float sumaParcial = pilaNumeros.pop();
        while (!pilaNumeros.isEmpty()) {
            sumaParcial = sumaParcial + pilaNumeros.pop();
        }

        return sumaParcial;
    };

    MultipleMathOperation restaMultiple = (LinkedList<Float> pilaNumeros) -> {
        float sumaParcial = pilaNumeros.pop();
        while (!pilaNumeros.isEmpty()) {
            sumaParcial = sumaParcial - pilaNumeros.pop();
        }
        return sumaParcial;
    };

    MultipleMathOperation multiplicacionMultiple = (LinkedList<Float> pilaNumeros) -> {
        float sumaParcial = pilaNumeros.pop();
        while (!pilaNumeros.isEmpty()) {
            sumaParcial = sumaParcial * pilaNumeros.pop();
        }
        return sumaParcial;
    };

    MultipleMathOperation divisionMultiple = (LinkedList<Float> pilaNumeros) -> {
        float sumaParcial = pilaNumeros.pop();
        while (!pilaNumeros.isEmpty()) {
            sumaParcial = sumaParcial / pilaNumeros.pop();
        }
        return sumaParcial;
    };

    MultipleMathOperation mod = (LinkedList<Float> pilaNumeros) -> {
        float sumaParcial = pilaNumeros.pop();
        while (!pilaNumeros.isEmpty()) {
            sumaParcial = sumaParcial % pilaNumeros.pop();
        }
        return sumaParcial;
    };

    public RPNCalculator() {

        operadores = new Hashtable<>();
        pilaNumeros = new LinkedList<>();

    }

    public float eval(String expression) {

        if (expression == null) {
            throw new IllegalArgumentException();
        }

        operadores.put("+", sumaMultiple);
        operadores.put("-", restaMultiple);
        operadores.put("*", multiplicacionMultiple);
        operadores.put("/", divisionMultiple);
        operadores.put("++", sumaMultiple);
        operadores.put("--", restaMultiple);
        operadores.put("**", multiplicacionMultiple);
        operadores.put("//", divisionMultiple);
        operadores.put("MOD", mod);

        String[] expressionSplit = expression.split(" ");

        for (int i = 0; i < expressionSplit.length; i++) {

            while (!operadores.containsKey(expressionSplit[i])) {
                pilaNumeros.addLast(Float.parseFloat(expressionSplit[i]));
                i++;
            }
            pilaNumeros.addLast(operateMultiple(pilaNumeros, operadores.get(expressionSplit[i])));

        }

        return pilaNumeros.pop();

    }

}
